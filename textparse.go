package textparse

import (
	"strconv"
	"strings"
)

// Itemize takes the substring starting immediately after `start` and ending before `end` from `input` and splits it using `sep`. Returns the result of the split or nil if either `start` or `end` is not found in `input`. Contrary to strings.Split, Itemize returns an empty slice if `sep` is not found and no splits were made.
func Itemize(input string, start string, end string, sep string) []string {
	input = Take(input, start, end)
	if input == "" {
		return nil
	}
	result := strings.Split(input, sep)
	if len(result) == 1 && result[0] == input {
		return []string{}
	}
	return result
}

// Take returns the substring starting immediately after `start` and ending before `end` from `input`, or an empty string if either `start` or `end` is not found in `input`. If `start` of `end` are left blank it means beginning of `input` and end of `input` respectively.
func Take(input string, start string, end string) string {
	pos := 0
	if start != "" {
		pos = strings.Index(input, start)
	}
	if pos < 0 {
		return ""
	}
	pos += len(start)
	input = input[pos:]
	pos = len(input)
	if end != "" {
		pos = strings.Index(input, end)
	}
	if pos < 0 {
		return ""
	}
	return input[:pos]
}

// Clean trims `input` from <space>, \n and \t.
func Clean(input string) string {
	return strings.Trim(input, " \n\t")
}

// Float returns `input` converted to a float64, or 0.0 if conversion was not possible.
func Float(input string) float64 {
	if input == "" {
		return 0.0
	}
	value, err := strconv.ParseFloat(input, 64)
	if err != nil {
		return 0.0
	}
	return value
}

// Integer returns `input` converted to an int, or 0 if conversion was not possible.
func Integer(input string) int {
	if input == "" {
		return 0
	}
	value, err := strconv.Atoi(input)
	if err != nil {
		return 0
	}
	return value
}

// IntegerSlice returns `input` converted to an []int, or an empty slice if conversion was not possible.
func IntegerSlice(input string) []int {
	result := make([]int, 0)
	for _, value := range strings.Split(input, ",") {
		result = append(result, Integer(value))
	}
	return result
}
