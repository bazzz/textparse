package textparse

import "testing"

func TestTake(t *testing.T) {
	input := "zzzaaabbb"
	expected := "aaa"
	actual := Take(input, "zzz", "bbb")
	if actual != expected {
		t.Fatal(actual, "!=", expected)
	}
}

func TestTakeFromStart(t *testing.T) {
	input := "zzzaaabbb"
	expected := "zzzaaa"
	actual := Take(input, "", "bbb")
	if actual != expected {
		t.Fatal(actual, "!=", expected)
	}
}

func TestTakeUntilEnd(t *testing.T) {
	input := "zzzaaabbb"
	expected := "aaabbb"
	actual := Take(input, "zzz", "")
	if actual != expected {
		t.Fatal(actual, "!=", expected)
	}
}

func TestItemize(t *testing.T) {
	input := "zzza a a abbb"
	expected := []string{"a", "a", "a", "a"}
	actual := Itemize(input, "zzz", "bbb", " ")
	if len(actual) != len(expected) {
		t.Fatal("len actual", len(actual), "!= len expected", len(expected))
	}
	for i := 0; i < len(actual); i++ {
		if actual[i] != expected[i] {
			t.Fatal("index", i, ":", actual[i], "!=", expected[i])
		}
	}
}
